package gioter

import (
	"io"
	"os"
	"testing"
	"time"

	"codeberg.org/jkhaak/entsoe"
)

const (
	EXAMPLE_ENTSO_DATA_1 = "resources/example-entsoe-1.xml"
	EXAMPLE_ENTSO_DATA_2 = "resources/example-entsoe-2.xml"
)

var ENSTO_XML []string = []string{EXAMPLE_ENTSO_DATA_1, EXAMPLE_ENTSO_DATA_2}

func readXML(path string) entsoe.EntsoePrices {
	r, _ := os.Open(path)
	defer r.Close()
	xml, _ := io.ReadAll(r)
	e, _ := entsoe.FromXml(xml)
	return e
}

func TestElectricPrices(t *testing.T) {
	c := ConfigElectric{
		Hours:  6,
		Token:  "",
		Domain: "",
	}
	ep := NewElectricProvider(c)
	var startTime time.Time

	for i, path := range ENSTO_XML {
		prices := readXML(path)
		if i == 0 {
			startTime = prices.StartTime
		}
		ep.add(prices)
	}

	data := []struct {
		offsetHours int
		expected    bool
	}{
		// start 2024-11-21 23:00
		{0, true},
		{1, true},
		{2, true},
		{3, true},
		{4, true},
		{5, false},
		{6, false},
		// time 2024-11-22 22:00
		{23, true},

		// time 2024-11-22 05:00
		{24 + 6, true},
		{24 + 7, false},
		// time 2024-11-22 11:00
		{24 + 12, false},
		{24 + 13, true},
		{24 + 14, false},
		// time 2024-11-22 19:00
		{24 + 20, true},
		{24 + 21, true},
		{24 + 22, true},
		{24 + 23, true},
	}

	for _, d := range data {
		testTime := startTime.Add(time.Hour * time.Duration(d.offsetHours))
		result := ep.IsCheap(testTime)

		if result != d.expected {
			t.Errorf("IsCheap calculated incorrectly hours at %s; want %t; got %t", testTime.Format(time.DateTime), d.expected, result)
		}
	}
}

func TestElectricConfig(t *testing.T) {
	data := []struct {
		name   string
		hours  int
		token  string
		domain string
	}{
		{"zero hours", 0, "token", "domain"},
		{"zero token", 6, "", "domain"},
		{"zero domain", 6, "token", ""},
	}

	for _, d := range data {
		t.Run(d.name, func(t *testing.T) {
			c := ConfigElectric{
				Hours:  d.hours,
				Token:  d.token,
				Domain: d.domain,
			}
			err := checkElectricConfig(c)

			if err == nil {
				t.Errorf("Expected error")
			}
		})
	}
}
