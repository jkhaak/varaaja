package gioter

import (
	"testing"
	"time"
)

const (
	callNoop      = "noop"
	calledIsCheap = "func e.IsCheap()"
	calledState   = "func m.State()"
	callSetOn     = "func m.SetOn()"
	callSetOff    = "func m.SetOff()"
)

type Mock struct {
	s                    map[string]bool
	isCheapReturnValue   bool
	mqttStateReturnValue bool
}

func NewMock() Mock {
	s := make(map[string]bool)
	return Mock{
		s: s,
	}
}

func (m *Mock) IsCheap(t time.Time) bool {
	m.s[calledIsCheap] = true
	return m.isCheapReturnValue
}

func (m *Mock) Resolution() time.Duration {
	return time.Hour
}

func (m Mock) State() bool {
	m.s[calledState] = true
	return m.mqttStateReturnValue
}

func (m Mock) Set(state bool) {
	if state {
		m.s[callSetOn] = true
	} else {
		m.s[callSetOff] = true
	}
}

func (m Mock) Disconnect() {}

func (m Mock) reset() {
	clear(m.s)
}

func (m *Mock) setReturnValues(isCheap, state bool) {
	m.isCheapReturnValue = isCheap
	m.mqttStateReturnValue = state
}

func TestGioterRun(t *testing.T) {
	m := NewMock()
	v := NewGioter(&m, &m)

	s, ok := m.s[calledIsCheap]
	if ok && s {
		t.Errorf("expected v.run to not call: %s", calledIsCheap)
	}

	s, ok = m.s[calledState]
	if ok && s {
		t.Errorf("expected v.run to not call: %s", calledState)
	}

	v.run()

	s = m.s[calledIsCheap]
	if !s {
		t.Errorf("expected v.run to call: %s", calledIsCheap)
	}

	s = m.s[calledState]
	if !s {
		t.Errorf("expected v.run to call: %s", calledState)
	}

	data := []struct {
		name     string
		isCheap  bool
		state    bool
		callFunc string
	}{
		{"cheap and state off", true, false, callSetOn},
		{"not cheap and state on", false, true, callSetOff},
		{"cheap and state on", true, true, callNoop},
		{"not cheap and state off", false, false, callNoop},
	}

	for _, d := range data {
		t.Run(d.name, func(t *testing.T) {
			m.reset()
			m.setReturnValues(d.isCheap, d.state)
			v.run()

			s := m.s[d.callFunc]
			if d.callFunc == callNoop {
				_, onOk := m.s[callSetOn]
				_, offOk := m.s[callSetOff]
				if onOk || offOk {
					t.Errorf("expected v.run to not call anything")
				}
			} else if !s {
				t.Errorf("expected v.run to call: %s; callState %v", d.callFunc, m.s)
			}
		})
	}
}
