package gioter

import (
	"log/slog"
	"os"
	"time"
)

type Config struct {
	Entsoe ConfigElectric `json:"entsoe"`
	Shelly ConfigShelly   `json:"shelly"`
}

type Gioter struct {
	e Electric
	s Shelly
}

func NewGioter(e Electric, s Shelly) Gioter {
	return Gioter{e: e, s: s}
}

func (g Gioter) run() {
	slog.Info("Main: checking current state")
	i := g.e.IsCheap(time.Now())
	s := g.s.State()

	switch {
	case i && !s:
		slog.Info("Main: set on", "isCheap", i, "state", s)
		g.s.Set(true)
	case !i && s:
		slog.Info("Main: set off", "isCheap", i, "state", s)
		g.s.Set(false)
	default:
		slog.Info("Main: do nothing")
	}
}

func nextCycle(t time.Time, r time.Duration) time.Time {
	n := t.Round(r)

	if n.Before(t) {
		n = n.Add(r)
	}

	return n
}

func Run(c Config) {
	err := checkElectricConfig(c.Entsoe)
	if err != nil {
		slog.Error("Main: invalid entsoe config", "error", err)
		os.Exit(1)
	}

	e := NewElectricProvider(c.Entsoe)
	if err != nil {
		slog.Error("Main: unexpected error while connecting to MQTT", "error", err)
		os.Exit(1)
	}

	s := NewShellySwitch(c.Shelly)
	g := NewGioter(&e, s)

	slog.Info("Main: on boot run")
	g.run()

	resolution := g.e.Resolution()

	// sleep till next cycle
	now := time.Now()
	next := nextCycle(now, resolution)
	asleep := next.Sub(now)
	slog.Info("Main: will now sleep", "until", next, "asleep", asleep, "resolution", resolution)
	time.Sleep(asleep)

	slog.Info("Main: doing second run")
	g.run()

	slog.Info("Main: initializing ticker")
	t := time.Tick(resolution)
	for range t {
		g.run()
	}
}
