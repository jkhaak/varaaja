vet:
  go fmt ./...
  go vet ./...
  go mod tidy

clean:
  rm -rf bin
  rm -rf target

test:
  go test

pre-build: vet test

build-cmd:
  mkdir -p bin
  go build -o bin ./cmd/...

build-target os arch: pre-build
  mkdir -p target/{{os}}_{{arch}}
  GOOS={{os}} GOARCH={{arch}} go build -o target/{{os}}_{{arch}}  ./cmd/...

build: pre-build build-cmd
