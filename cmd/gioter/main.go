package main

import (
	"encoding/json"
	"log/slog"
	"os"

	"codeberg.org/jkhaak/gioter"
)

func readConfig() gioter.Config {
	r, err := os.Open("config.json")
	if err != nil {
		slog.Error("Config: error while opening config file", "error", err)
		os.Exit(1)
	}
	defer r.Close()

	var c gioter.Config
	err = json.NewDecoder(r).Decode(&c)
	if err != nil {
		slog.Error("Config: error while parsing config file", "error", err)
		os.Exit(1)
	}

	return c
}

func main() {
	c := readConfig()

	gioter.Run(c)
}
