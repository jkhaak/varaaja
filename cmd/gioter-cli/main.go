package main

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"os"

	"codeberg.org/jkhaak/gioter"
)

func readConfig() gioter.Config {
	r, err := os.Open("config.json")
	if err != nil {
		slog.Error("Config: error while opening config file", "error", err)
		os.Exit(1)
	}
	defer r.Close()

	var c gioter.Config
	err = json.NewDecoder(r).Decode(&c)
	if err != nil {
		slog.Error("Config: error while parsing config file", "error", err)
		os.Exit(1)
	}

	return c
}

func run(c gioter.Config) {
	s := gioter.NewShellySwitch(c.Shelly)

	var op string

loop:
	for {
		fmt.Print("command (state,on,off,q): ")
		fmt.Scanln(&op)

		switch op {
		case "state":
			fmt.Printf("current state: %t\n", s.State())
		case "on":
			fmt.Println("turning on...")
			s.Set(true)
		case "off":
			fmt.Println("turning off...")
			s.Set(false)
		case "q":
			break loop
		}
	}
}

func main() {
	c := readConfig()

	run(c)
}
