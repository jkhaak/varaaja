package gioter

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"time"
)

const (
	CONTENT_TYPE_JSON = "application/json"
	SHELLY_GET_STATUS = "Switch.GetStatus"
	SHELLY_SET_STATUS = "Switch.Set"
)

type ConfigShelly struct {
	SwitchID int    `json:"switchId"`
	Addr     string `json:"addr"`
}

type Shelly interface {
	State() bool
	Set(state bool)
}

type ShellySwitch struct {
	state    bool
	switchID int
	addr     string
}

type JsonRpcResponse struct {
	ID     int             `json:"id"`
	Src    string          `json:"src"`
	Result json.RawMessage `json:"result"`
}

type JsonRpcRequest struct {
	ID     int    `json:"id"`
	Method string `json:"method"`
	Params any    `json:"params,omitempty"`
}

type shellySet struct {
	ID int  `json:"id"`
	On bool `json:"on"`
}

type shellySetResponse struct {
	WasOn bool `json:"was_on"`
}

type shellyGetStatus struct {
	ID int `json:"id"`
}

type shellyGetStatusResponse struct {
	ID          int    `json:"id"`
	Source      string `json:"source"`
	Output      bool   `json:"output"`
	Temperature struct {
		Celcius    float64 `json:"tC"`
		Fahrenheit float64 `json:"tF"`
	} `json:"temperature"`
}

func NewShellySwitch(c ConfigShelly) *ShellySwitch {
	return &ShellySwitch{
		switchID: c.SwitchID,
		addr:     c.Addr,
	}
}

func (s ShellySwitch) send(method string, params any, response any) error {
	payload := JsonRpcRequest{
		ID:     int(time.Now().Unix()),
		Method: method,
		Params: params,
	}

	body, err := json.Marshal(payload)
	if err != nil {
		return fmt.Errorf("failed to marshal the payload: %w", err)
	}

	url := fmt.Sprintf("http://%s/rpc", s.addr)
	slog.Info("Shelly: sending request to shelly", "url", url, "payload", payload)
	r, err := http.Post(url, CONTENT_TYPE_JSON, bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("failed to send payload to shelly: %w", err)
	}
	defer r.Body.Close()

	rBody, err := io.ReadAll(r.Body)
	if err != nil {
		return fmt.Errorf("failed to read request body: %w", err)
	}

	var jsonRpc JsonRpcResponse
	if err := json.Unmarshal(rBody, &jsonRpc); err != nil {
		return fmt.Errorf("failed to unmarshal JsonRPC: %w", err)
	}

	if err := json.Unmarshal(jsonRpc.Result, &response); err != nil {
		return fmt.Errorf("failed to unmarshal result: %w", err)
	}

	return nil
}

func (s ShellySwitch) State() bool {
	var resp shellyGetStatusResponse
	payload := shellyGetStatus{ID: s.switchID}
	s.send(SHELLY_GET_STATUS, payload, &resp)
	return resp.Output
}

func (s ShellySwitch) Set(state bool) {
	var resp shellySetResponse
	payload := shellySet{
		ID: s.switchID,
		On: state,
	}
	s.send(SHELLY_SET_STATUS, payload, &resp)
}
