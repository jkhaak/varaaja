package gioter

import (
	"container/ring"
	"fmt"
	"log/slog"
	"sort"
	"time"

	"codeberg.org/jkhaak/entsoe"
)

const pricesSetsInMemory = 7

type Electric interface {
	IsCheap(time time.Time) bool
	Resolution() time.Duration
}

type ConfigElectric struct {
	Hours  int    `json:"hours"`
	Token  string `json:"token"`
	Domain string `json:"domain"`
}

func checkElectricConfig(c ConfigElectric) error {
	if c.Hours == 0 {
		return fmt.Errorf("config has zero hours")
	}

	if c.Token == "" {
		return fmt.Errorf("empty token")
	}

	if c.Domain == "" {
		return fmt.Errorf("empty domain")
	}

	return nil
}

type ElectricProvider struct {
	entsoApi   entsoe.EntsoePriceApi
	minutes    int
	prices     *ring.Ring
	resolution time.Duration
}

func NewElectricProvider(c ConfigElectric) ElectricProvider {
	r := ring.New(pricesSetsInMemory)

	var resolution time.Duration

	return ElectricProvider{
		entsoApi:   entsoe.NewEntsoePriceApi(c.Token, c.Domain),
		minutes:    c.Hours * 60,
		prices:     r,
		resolution: resolution,
	}
}

func (e *ElectricProvider) findPriceSet(t time.Time) (entsoe.EntsoePrices, error) {
	var pSet *entsoe.EntsoePrices

	e.prices.Do(func(p any) {
		if p == nil {
			return
		}

		localPSet := p.(entsoe.EntsoePrices)
		if (t.Equal(localPSet.StartTime) || t.After(localPSet.StartTime)) && (t.Equal(localPSet.EndTime) || t.Before(localPSet.EndTime)) {
			pSet = &localPSet
		}
	})

	// set not in memory, try to fetch it
	if pSet == nil {
		slog.Info("Electric: trying to fetch energy prices")
		p, err := e.entsoApi.GetEnergyPrices(t)
		if err != nil {
			slog.Error("Electric: unable to fetch prices", "error", err)
			var zero entsoe.EntsoePrices
			return zero, fmt.Errorf("not found")
		}
		pSet = &p

		slog.Info("Electric: got new prices", "priceSet", pSet)
		// set resolution
		e.resolution = time.Duration(pSet.ResolutionMin) * time.Minute

		e.add(p)
	}

	return *pSet, nil
}

func (e *ElectricProvider) IsCheap(t time.Time) bool {
	priceSet, err := e.findPriceSet(t)
	if err != nil {
		slog.Error("Electric: price set not found for given time", "time", t, "error", err)
		return false
	}

	var data []struct {
		offset int
		price  float64
	}

	resolution := priceSet.ResolutionMin

	for i, p := range priceSet.Prices {
		data = append(data, struct {
			offset int
			price  float64
		}{
			offset: i * resolution,
			price:  p,
		})
	}

	sort.Slice(data, func(i, j int) bool {
		return data[i].price < data[j].price
	})

	offset := int(t.Sub(priceSet.StartTime).Minutes())

	for i := 0; i*resolution < e.minutes; i++ {
		if data[i].offset == offset {
			return true
		}
	}

	return false
}

func (e *ElectricProvider) Resolution() time.Duration {
	var zero time.Duration

	if zero == e.resolution {
		// get a price set
		slog.Info("Electric: resolution is at zero, try to fetch prices")
		e.findPriceSet(time.Now())
	}

	return e.resolution
}

func (e *ElectricProvider) add(ep entsoe.EntsoePrices) *ElectricProvider {
	e.prices.Value = ep
	e.prices = e.prices.Next()

	return e
}
